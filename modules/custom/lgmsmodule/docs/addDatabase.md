# Adding a DataBase to the Box

__Creation Date:__ March 31, 2024
__Created By:__ Mohammed Amaan



***




## Navigate to the Box where the database is to be added


### 1. Click on Add/Reuse Database
![Step 1 screenshot](https://images.tango.us/workflows/1c359512-d3ef-4064-ac76-b549ee754a93/steps/6f935eaf-3cdf-4aa1-9a3d-facef694bec6/70c2d897-1595-43b5-a909-65cd11866854.png?crop=focalpoint&fit=crop&fp-x=0.4042&fp-y=0.6515&fp-z=2.2481&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=405&mark-y=457&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz0zOTEmaD02NyZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 2. Click on Database Title:

Enter the Title

![Step 2 screenshot](https://images.tango.us/workflows/1c359512-d3ef-4064-ac76-b549ee754a93/steps/7a1b7889-c4f1-4bfd-b190-77998ce095a4/2db26591-ae26-4ee8-be9b-e49ea23fd239.png?crop=focalpoint&fit=crop&fp-x=0.5005&fp-y=0.2565&fp-z=1.1706&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=102&mark-y=264&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz05OTcmaD02MSZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 3. Click on Link Text:

Enter the Link text

![Step 3 screenshot](https://images.tango.us/workflows/1c359512-d3ef-4064-ac76-b549ee754a93/steps/08de9ad7-7656-4906-9766-4ae9a9d55800/abba0851-38e0-4a6d-9c45-0fe7dbec801a.png?crop=focalpoint&fit=crop&fp-x=0.5005&fp-y=0.3462&fp-z=1.1706&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=102&mark-y=367&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz05OTcmaD02MSZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 4. Click on Database Link

Enter the Database Link

![Step 4 screenshot](https://images.tango.us/workflows/1c359512-d3ef-4064-ac76-b549ee754a93/steps/d8cacaaa-fef5-4042-9fe2-8fe4444939a2/3c98833e-cf75-4b3d-811d-cfc95cc57a11.png?crop=focalpoint&fit=crop&fp-x=0.5005&fp-y=0.4360&fp-z=1.1706&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=102&mark-y=460&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz05OTcmaD02MSZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 5. Check Include Proxy

Proxy gives the you the ability to add a url proxy before being redirected to the database.

![Step 5 screenshot](https://images.tango.us/workflows/1c359512-d3ef-4064-ac76-b549ee754a93/steps/fdaf0d51-477b-4401-be0c-dd6e949f807b/e02ddcaf-bfaf-4dcd-b102-f560a462454d.png?crop=focalpoint&fit=crop&fp-x=0.1554&fp-y=0.5292&fp-z=3.0905&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=540&mark-y=454&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz03MSZoPTcxJmZpdD1jcm9wJmNvcm5lci1yYWRpdXM9MTA%3D)


### 6. Click on Brief Description

Enter a Description

![Step 6 screenshot](https://images.tango.us/workflows/1c359512-d3ef-4064-ac76-b549ee754a93/steps/71ddb844-ffe3-4eb4-93f3-65036c7b8f5a/96dac1a6-d441-4e56-afa9-5d9d48347a19.png?crop=focalpoint&fit=crop&fp-x=0.5005&fp-y=0.5651&fp-z=1.1706&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=102&mark-y=460&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz05OTcmaD02MSZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 7. Click on Editor editing area

Here you can add additional instructions or details about the database

![Step 7 screenshot](https://images.tango.us/workflows/1c359512-d3ef-4064-ac76-b549ee754a93/steps/12b80f61-7823-416e-b780-492f425f60fe/ea707a1c-4b41-48f5-8a7c-a0228fd42a7e.png?crop=focalpoint&fit=crop&fp-x=0.5005&fp-y=0.6229&fp-z=1.1706&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=102&mark-y=413&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz05OTcmaD0yNjgmZml0PWNyb3AmY29ybmVyLXJhZGl1cz0xMA%3D%3D)


### 8. Click on Save to add the guide to your box
![Step 8 screenshot](https://images.tango.us/workflows/1c359512-d3ef-4064-ac76-b549ee754a93/steps/3390ce20-f74b-4c65-aea3-a41ba36cd732/06e5e58b-b44a-4e45-993c-2ca409b8af09.png?crop=focalpoint&fit=crop&fp-x=0.1962&fp-y=0.8704&fp-z=2.6918&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=485&mark-y=560&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz0yMzEmaD0xNTcmZml0PWNyb3AmY29ybmVyLXJhZGl1cz0xMA%3D%3D)

