# Reuse an HTML

This is the place where text block or HTML blocks can be reused from the same guide or some other guide

__Creation Date:__ March 31, 2024
__Created By:__ Mohammed Amaan



***




## Navigate to the box that is going to reuse the content


### 1. Click on Add/Reuse HTML
![Step 1 screenshot](https://images.tango.us/workflows/196d5909-5060-4564-b332-cfb65402a66e/steps/de79b58d-8260-44bf-a7ff-e34dcb308b7d/ef79f322-1097-46b3-8bc1-bd576499a772.png?crop=focalpoint&fit=crop&fp-x=0.3932&fp-y=0.6470&fp-z=2.3651&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=426&mark-y=455&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz0zNDkmaD03MCZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 2. Click on Reuse HTML
![Step 2 screenshot](https://images.tango.us/workflows/196d5909-5060-4564-b332-cfb65402a66e/steps/adbf34b6-94fb-43c4-9c60-ab8b99d59864/db7f8c99-558f-4d9a-90e2-d5c00c2c846f.png?crop=focalpoint&fit=crop&fp-x=0.2961&fp-y=0.1672&fp-z=2.4068&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=433&mark-y=334&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz0zMzQmaD0xMjImZml0PWNyb3AmY29ybmVyLXJhZGl1cz0xMA%3D%3D)


### 3. Select the required block from Select HTML Item
![Step 3 screenshot](https://images.tango.us/workflows/196d5909-5060-4564-b332-cfb65402a66e/steps/8b801791-daba-4836-bc28-829ea0311d5b/fae176e5-57f5-40c5-a332-14b02cb6f2b8.png?crop=focalpoint&fit=crop&fp-x=0.5005&fp-y=0.2576&fp-z=1.1706&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=102&mark-y=265&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz05OTcmaD02MSZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 4. Click on Save
![Step 4 screenshot](https://images.tango.us/workflows/196d5909-5060-4564-b332-cfb65402a66e/steps/505fb517-5c70-47f9-b546-01dc0dd85435/73023423-ec38-4a6f-8cb5-21d92aa779ef.png?crop=focalpoint&fit=crop&fp-x=0.1962&fp-y=0.8861&fp-z=2.6918&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=485&mark-y=601&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz0yMzEmaD0xNTcmZml0PWNyb3AmY29ybmVyLXJhZGl1cz0xMA%3D%3D)

