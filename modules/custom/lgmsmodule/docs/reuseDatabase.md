# Reuse Database

Allows one to Reuse and existing database content type from a box

__Creation Date:__ March 31, 2024
__Created By:__ Mohammed Amaan



***




## Navigate to the required box.


### 1. Click on Add/Reuse Database
![Step 1 screenshot](https://images.tango.us/workflows/83387a65-9c51-46cf-8b06-236e0217b73d/steps/77f3babc-86eb-4a6b-8041-bc94518459c2/2d9326be-af1d-4922-9bd3-9022393744bd.png?crop=focalpoint&fit=crop&fp-x=0.4042&fp-y=0.6515&fp-z=2.2481&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=405&mark-y=457&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz0zOTEmaD02NyZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 2. Click on Reuse Database
![Step 2 screenshot](https://images.tango.us/workflows/83387a65-9c51-46cf-8b06-236e0217b73d/steps/a797e346-ff25-40bf-b640-bce414d4bc07/cd901e96-1d97-4579-9df9-8dbd559800c6.png?crop=focalpoint&fit=crop&fp-x=0.3304&fp-y=0.1661&fp-z=2.2810&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=411&mark-y=314&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz0zNzkmaD0xMTUmZml0PWNyb3AmY29ybmVyLXJhZGl1cz0xMA%3D%3D)


### 3. Select the desired Database
![Step 3 screenshot](https://images.tango.us/workflows/83387a65-9c51-46cf-8b06-236e0217b73d/steps/974de293-1d00-4c4f-bdb1-0cd60dce2953/b0ac0663-6d83-4f40-93ee-be8d193a4da0.png?crop=focalpoint&fit=crop&fp-x=0.5005&fp-y=0.2565&fp-z=1.1706&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=102&mark-y=264&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz05OTcmaD02MSZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 4. Click on Save
![Step 4 screenshot](https://images.tango.us/workflows/83387a65-9c51-46cf-8b06-236e0217b73d/steps/ed00fcca-8f9e-4927-a1e8-3300a0c70d6f/fe8d172e-8f15-4f90-abdf-a9ec9b9d75fb.png?crop=focalpoint&fit=crop&fp-x=0.1962&fp-y=0.8704&fp-z=2.6918&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=485&mark-y=560&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz0yMzEmaD0xNTcmZml0PWNyb3AmY29ybmVyLXJhZGl1cz0xMA%3D%3D)

