
# Adding HTML/ Text Content to Your Box
__Creation Date:__ March 31, 2024
__Created By:__ Mohammed Amaan




***




##  Navigate to the required Page and then the box


### 1. This is what the Box will Look like without any content in it
![Step 1 screenshot](https://images.tango.us/workflows/80425ed0-4b37-44c7-8f41-bbe19d009ed3/steps/c0b98ecd-f9f8-43aa-a971-63d35b97feb7/82e7c59b-c95f-4e20-8745-c66e6e414c96.png?crop=focalpoint&fit=crop&fp-x=0.6214&fp-y=0.6066&fp-z=1.4224&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=46&mark-y=303&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz0xMTA4Jmg9Mzc0JmZpdD1jcm9wJmNvcm5lci1yYWRpdXM9MTA%3D)


### 2. Click on Add/Reuse HTML to add Plain text or HTML content
![Step 2 screenshot](https://images.tango.us/workflows/80425ed0-4b37-44c7-8f41-bbe19d009ed3/steps/bee8f840-00d2-4541-83b3-5520fa5ca621/7933b6a5-2776-450d-983d-1e2864e3509d.png?crop=focalpoint&fit=crop&fp-x=0.3932&fp-y=0.5976&fp-z=2.3651&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=426&mark-y=455&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz0zNDkmaD03MCZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 3. Add Title
![Step 3 screenshot](https://images.tango.us/workflows/80425ed0-4b37-44c7-8f41-bbe19d009ed3/steps/bdc2bb58-3fbc-4c8e-bc3c-8bd4c63ed45a/8015f4ef-4dbc-456a-832e-181d9466b4ec.png?crop=focalpoint&fit=crop&fp-x=0.5005&fp-y=0.2576&fp-z=1.1706&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=102&mark-y=265&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz05OTcmaD02MSZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 4. In the Body text box is where you will add the content.

By Default Basic HTML is selected

![Step 4 screenshot](https://images.tango.us/workflows/80425ed0-4b37-44c7-8f41-bbe19d009ed3/steps/097a09ac-c363-42af-835b-242b6cebfbe2/1a01e8b3-83b1-4848-8a3d-18d1be3f366d.png?crop=focalpoint&fit=crop&fp-x=0.5005&fp-y=0.4826&fp-z=1.1706&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=102&mark-y=356&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz05OTcmaD0yNjgmZml0PWNyb3AmY29ybmVyLXJhZGl1cz0xMA%3D%3D)


### 5. You have the option to change it to full HTML or Restricted HTML
![Step 5 screenshot](https://images.tango.us/workflows/80425ed0-4b37-44c7-8f41-bbe19d009ed3/steps/464ed90c-35f9-4900-b93a-f26c88470c42/6ca23239-c171-44dd-8e4a-54967b3c48b8.png?crop=focalpoint&fit=crop&fp-x=0.5005&fp-y=0.5932&fp-z=1.1706&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=102&mark-y=483&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz05OTcmaD02MSZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 6. Click on Save
![Step 6 screenshot](https://images.tango.us/workflows/80425ed0-4b37-44c7-8f41-bbe19d009ed3/steps/086224cf-3251-45c6-a184-d20bf7e4800e/735bf073-ed2a-4656-938c-e7f063f4ec14.png?crop=focalpoint&fit=crop&fp-x=0.1962&fp-y=0.8692&fp-z=2.6918&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=485&mark-y=557&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz0yMzEmaD0xNTcmZml0PWNyb3AmY29ybmVyLXJhZGl1cz0xMA%3D%3D)
