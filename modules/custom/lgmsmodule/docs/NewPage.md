# How to Add a Page To a Guide

__Creation Date:__ March 31, 2024
__Created By:__ Mohammed Amaan




***




## Navigate to the Guide from the Dashboard or Create a new guide


### 1. Click on Create/Reuse Guide Page
![Step 1 screenshot](https://images.tango.us/workflows/718b2df1-f17d-40cc-8359-44062a5011d8/steps/8b7fe742-3586-4071-a7ba-93e05b2bb710/ad7725ff-6685-4be4-bc3a-217deb2d92d6.png?crop=focalpoint&fit=crop&fp-x=0.1425&fp-y=0.3827&fp-z=1.9754&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=93&mark-y=455&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz00ODkmaD03MCZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 2. Input your title
![Step 2 screenshot](https://images.tango.us/workflows/718b2df1-f17d-40cc-8359-44062a5011d8/steps/5581320d-59d1-4609-9c5f-15d1aabc60ba/c8dbbd8f-1137-4b8f-8083-7b01a4a14b7a.png?crop=focalpoint&fit=crop&fp-x=0.5005&fp-y=0.3013&fp-z=1.1706&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=102&mark-y=315&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz05OTcmaD02MSZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 3. Input the Description

If you would like no description for the page select the Hide Description option.

![Step 3 screenshot](https://images.tango.us/workflows/718b2df1-f17d-40cc-8359-44062a5011d8/steps/9b4377ca-aa3b-440f-8246-68f6ab18a1eb/9b75dae1-d207-43f6-9d18-8b5c3a428c19.png?crop=focalpoint&fit=crop&fp-x=0.5005&fp-y=0.5814&fp-z=1.1706&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=102&mark-y=366&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz05OTcmaD0yNjgmZml0PWNyb3AmY29ybmVyLXJhZGl1cz0xMA%3D%3D)


### 4. Click on Create Page to Have your Page Created
![Step 4 screenshot](https://images.tango.us/workflows/718b2df1-f17d-40cc-8359-44062a5011d8/steps/d387f0dd-6d40-4b87-a65d-af8a4b1130b0/c756c83f-c8ec-4213-a5b6-31961f2883d3.png?crop=focalpoint&fit=crop&fp-x=0.2209&fp-y=0.8704&fp-z=2.3754&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=428&mark-y=609&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz0zNDUmaD0xMzgmZml0PWNyb3AmY29ybmVyLXJhZGl1cz0xMA%3D%3D)


### 5. Navigation to Recently Created Page

To navigate to the recently created page click on the the page title to be redirected to the page.

![Step 5 screenshot](https://images.tango.us/workflows/718b2df1-f17d-40cc-8359-44062a5011d8/steps/7713af79-8b1b-4b3a-b7be-87e7d171033e/e1c585a9-1865-4271-91d5-8f76ee54bc4d.png?crop=focalpoint&fit=crop&fp-x=0.1370&fp-y=0.6033&fp-z=2.1634&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=145&mark-y=458&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz00MjEmaD02NCZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 6. Here is what the Page looks Like after creation

You can start adding More Content by adding boxes.

![Step 6 screenshot](https://images.tango.us/workflows/718b2df1-f17d-40cc-8359-44062a5011d8/steps/ad66da39-5d27-4803-9a5d-f56bc2bcce1c/178159ef-3e67-431a-96a4-9b47bc9a053d.png?crop=focalpoint&fit=crop&fp-x=0.5000&fp-y=0.5982&fp-z=1.0037&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=2&mark-y=285&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz0xMTk2Jmg9NjAxJmZpdD1jcm9wJmNvcm5lci1yYWRpdXM9MTA%3D)


