# Creating A Box

__Creation Date:__ March 31, 2024
__Created By:__ Mohammed Amaan



***




## Navigate to the Page where you would like to add the box


### 1. Click on Create/Reuse Guide Box
![Step 1 screenshot](https://images.tango.us/workflows/a7aa6b7d-ab80-4419-ad9f-6463322ad59f/steps/079255f6-0e9f-4e02-8bc2-8182bf4035c5/3932ac41-b021-4cff-bf65-79a90b26c61e.png?crop=focalpoint&fit=crop&fp-x=0.3799&fp-y=0.6919&fp-z=2.1464&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=386&mark-y=458&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz00MjcmaD02NCZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 2. Enter Your Title
![Step 2 screenshot](https://images.tango.us/workflows/a7aa6b7d-ab80-4419-ad9f-6463322ad59f/steps/a102b58f-4d40-47b1-9ad0-2c7c56cb0c29/9fb9745b-ec03-471c-97da-63e41182e860.png?crop=focalpoint&fit=crop&fp-x=0.5005&fp-y=0.4910&fp-z=1.1706&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=102&mark-y=460&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz05OTcmaD02MSZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 3. Click on Save
![Step 3 screenshot](https://images.tango.us/workflows/a7aa6b7d-ab80-4419-ad9f-6463322ad59f/steps/46454f15-21e6-4e36-ae18-b073fda169be/c3bc9739-3f8a-4113-a962-e35790afc6c4.png?crop=focalpoint&fit=crop&fp-x=0.1962&fp-y=0.6347&fp-z=2.6918&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=485&mark-y=412&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz0yMzEmaD0xNTcmZml0PWNyb3AmY29ybmVyLXJhZGl1cz0xMA%3D%3D)

