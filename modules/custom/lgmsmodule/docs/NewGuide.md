# Steps to Create a New Guide

__Creation Date:__ March 25, 2024
__Created By:__ Mohammed Amaan

***


## [Library Guide Management System | lgms](http://localhost/LGMS/lgms)
Welcome to Library Guide Management System Setting up a new guide Help. Follow the steps below to create your first guide.


## 1. Click on My Dashboard
![Step 1 screenshot](https://images.tango.us/workflows/2433334b-218b-449e-ac47-cad62f893575/steps/d9e4bfb7-83b3-4425-94e2-5f60d9776602/a1194031-e14f-473b-ba43-431915d1ca37.png?crop=focalpoint&fit=crop&fp-x=0.8818&fp-y=0.3743&fp-z=2.8169&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=614&mark-y=414&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz0zNzImaD0xNTImZml0PWNyb3AmY29ybmVyLXJhZGl1cz0xMA%3D%3D)


## 2. Click on New
![Step 2 screenshot](https://images.tango.us/workflows/2433334b-218b-449e-ac47-cad62f893575/steps/705953b7-6adc-4787-b923-6b1b050d6398/973e0e93-ff3e-484f-ad14-86bcb0b18772.png?crop=focalpoint&fit=crop&fp-x=0.1329&fp-y=0.2593&fp-z=2.7599&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=337&mark-y=429&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz0yMDYmaD0xMjEmZml0PWNyb3AmY29ybmVyLXJhZGl1cz0xMA%3D%3D)


## 3. Type the name of the Subject in our case we will add Psychology
![Step 3 screenshot](https://images.tango.us/workflows/2433334b-218b-449e-ac47-cad62f893575/steps/893b7b96-7bf7-4994-aca4-fbc4f5fc230a/bdd969e3-9a31-477c-85a5-444c3662c3bf.png?crop=focalpoint&fit=crop&fp-x=0.5000&fp-y=0.4731&fp-z=1.0521&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=31&mark-y=461&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz0xMTM4Jmg9NTQmZml0PWNyb3AmY29ybmVyLXJhZGl1cz0xMA%3D%3D)


## 4. Type or Paste selected text into element
![Step 4 screenshot](https://images.tango.us/workflows/2433334b-218b-449e-ac47-cad62f893575/steps/50972923-a3cd-4d80-a107-371bd3cb1905/6532afde-cae7-4c6d-bd47-5ef34f5ae694.png?crop=focalpoint&fit=crop&fp-x=0.5000&fp-y=0.5039&fp-z=1.0521&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=31&mark-y=370&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz0xMTM4Jmg9MjQxJmZpdD1jcm9wJmNvcm5lci1yYWRpdXM9MTA%3D)


## 5. Check The Relevant subjects Psychology. Do not remember all you ? No problem you can add them later in the Edit tab.
![Step 5 screenshot](https://images.tango.us/workflows/2433334b-218b-449e-ac47-cad62f893575/steps/23f88a2d-c6d1-400f-9baa-c46979dd5724/cc1a8d57-37ba-43dc-b81d-b16c48ab1414.png?crop=focalpoint&fit=crop&fp-x=0.0591&fp-y=0.4618&fp-z=3.0905&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=184&mark-y=454&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz03MSZoPTcxJmZpdD1jcm9wJmNvcm5lci1yYWRpdXM9MTA%3D)


## 6. Check Economics
![Step 6 screenshot](https://images.tango.us/workflows/2433334b-218b-449e-ac47-cad62f893575/steps/175cc0f7-1f45-4e3b-bb50-53f706637d9a/b0ee66dc-38a7-4d77-8e87-4004aa1c354b.png?crop=focalpoint&fit=crop&fp-x=0.0591&fp-y=0.2250&fp-z=3.0905&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=184&mark-y=454&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz03MSZoPTcxJmZpdD1jcm9wJmNvcm5lci1yYWRpdXM9MTA%3D)


## 7. Select The Type of the guide

There are 4 options here to choose from

*   Course Guide

*   General Purpose Guide

*   Subject Guide

*   Topic Guide


These are not mandatory but helpful.

![Step 7 screenshot](https://images.tango.us/workflows/2433334b-218b-449e-ac47-cad62f893575/steps/255f65fd-8186-4858-9f66-d1c2853f1f5f/381f1d88-d933-4e27-8065-c334c623eb70.png?crop=focalpoint&fit=crop&fp-x=0.5000&fp-y=0.4304&fp-z=1.0521&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=31&mark-y=417&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz0xMTM4Jmg9NTQmZml0PWNyb3AmY29ybmVyLXJhZGl1cz0xMA%3D%3D)


## 8. You can Also add them to a group for better organization

You will have the option to choose from Group 1-4

![Step 8 screenshot](https://images.tango.us/workflows/2433334b-218b-449e-ac47-cad62f893575/steps/09f63e99-e410-4446-90f5-f252bc0c1862/a83770b5-b6f2-4455-a2b6-ef9258c9fc0c.png?crop=focalpoint&fit=crop&fp-x=0.5000&fp-y=0.5382&fp-z=1.0521&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=31&mark-y=477&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz0xMTM4Jmg9NTQmZml0PWNyb3AmY29ybmVyLXJhZGl1cz0xMA%3D%3D)


## 9. Lastly click save to publish the guide
![Step 9 screenshot](https://images.tango.us/workflows/2433334b-218b-449e-ac47-cad62f893575/steps/9ad58dab-5350-47b8-ade9-9496a64d7a40/2ba9f428-b133-4ea6-9f08-8c0789f00b25.png?crop=focalpoint&fit=crop&fp-x=0.0797&fp-y=0.8075&fp-z=2.7740&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=165&mark-y=415&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz0yMDEmaD0xNTAmZml0PWNyb3AmY29ybmVyLXJhZGl1cz0xMA%3D%3D)
