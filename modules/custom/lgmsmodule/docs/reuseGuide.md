# Reuse a Guide

__Creation Date:__ March 31, 2024
__Created By:__ Mohammed Amaan



***




## Navigate to the Dashboard Overview


### 1. Click on Reuse Guide
![Step 1 screenshot](https://images.tango.us/workflows/1215df59-c463-41b0-8598-f61e3c9ebf86/steps/99096531-94fe-4499-b913-b68bca5c73c6/a9a64162-3459-467a-bd4c-f3b745675475.png?crop=focalpoint&fit=crop&fp-x=0.2191&fp-y=0.2593&fp-z=2.4500&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=441&mark-y=436&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz0zMTgmaD0xMDgmZml0PWNyb3AmY29ybmVyLXJhZGl1cz0xMA%3D%3D)


### 2. Select the guide to be reused
![Step 2 screenshot](https://images.tango.us/workflows/1215df59-c463-41b0-8598-f61e3c9ebf86/steps/fd1b5b98-905d-464b-b24f-bff1aee2f7ad/3ec04f52-0b86-4156-b61d-e69222e3ad33.png?crop=focalpoint&fit=crop&fp-x=0.5000&fp-y=0.4035&fp-z=1.0521&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=31&mark-y=389&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz0xMTM4Jmg9NTQmZml0PWNyb3AmY29ybmVyLXJhZGl1cz0xMA%3D%3D)


### 3. Click on Reuse Guide
![Step 3 screenshot](https://images.tango.us/workflows/1215df59-c463-41b0-8598-f61e3c9ebf86/steps/4810fb0e-bed3-4bc9-91f2-656df3eba043/28cf6125-1c84-4507-bf09-b9df17770514.png?crop=focalpoint&fit=crop&fp-x=0.1123&fp-y=0.4843&fp-z=2.5006&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=187&mark-y=423&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz0zMDAmaD0xMzUmZml0PWNyb3AmY29ybmVyLXJhZGl1cz0xMA%3D%3D)


### 4. Here you can change the title as well as description of the guide before reusing it.
![Step 4 screenshot](https://images.tango.us/workflows/1215df59-c463-41b0-8598-f61e3c9ebf86/steps/4ef2a40c-72bf-4df9-a483-840adec33a2e/803cc487-8c30-4b2c-ab5a-0f6e6d0f5a4c.png?crop=focalpoint&fit=crop&fp-x=0.5000&fp-y=0.3406&fp-z=1.0521&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=31&mark-y=324&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz0xMTM4Jmg9NTQmZml0PWNyb3AmY29ybmVyLXJhZGl1cz0xMA%3D%3D)


### 5. Check or uncheck subjects that are related to the guide
![Step 5 screenshot](https://images.tango.us/workflows/1215df59-c463-41b0-8598-f61e3c9ebf86/steps/a1a0171a-d478-4885-b923-788791eb3d51/35481831-914a-4abb-aeef-9dd4771b6c07.png?crop=focalpoint&fit=crop&fp-x=0.0591&fp-y=0.3114&fp-z=3.0905&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=184&mark-y=454&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz03MSZoPTcxJmZpdD1jcm9wJmNvcm5lci1yYWRpdXM9MTA%3D)


### 6. Click on URL alias
![Step 6 screenshot](https://images.tango.us/workflows/1215df59-c463-41b0-8598-f61e3c9ebf86/steps/30b4d61e-e7fa-4bdc-8734-3f3ca7ebf448/bb7bf288-e6c7-4d29-8c01-962135fe58ec.png?crop=focalpoint&fit=crop&fp-x=0.1641&fp-y=0.4826&fp-z=1.9030&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=117&mark-y=398&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz01MTUmaD0xODQmZml0PWNyb3AmY29ybmVyLXJhZGl1cz0xMA%3D%3D)


### 7. Type new url alias as the previous one is in use by the original guide
![Step 7 screenshot](https://images.tango.us/workflows/1215df59-c463-41b0-8598-f61e3c9ebf86/steps/d2e2f1b7-ced9-4882-8ac5-d0a3bf741e28/2b0f411b-042e-41a2-92a5-fa6afeb32a9e.png?crop=focalpoint&fit=crop&fp-x=0.6100&fp-y=0.3283&fp-z=1.4132&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=61&mark-y=418&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz0xMDc3Jmg9NzMmZml0PWNyb3AmY29ybmVyLXJhZGl1cz0xMA%3D%3D)


### 8. Click on Save to publish the guide.
![Step 8 screenshot](https://images.tango.us/workflows/1215df59-c463-41b0-8598-f61e3c9ebf86/steps/bbc293e9-d200-4581-9ed4-8b30306667ca/5dd39d3c-b242-48fe-a25a-144f6872227f.png?crop=focalpoint&fit=crop&fp-x=0.0797&fp-y=0.7626&fp-z=2.7740&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=165&mark-y=415&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz0yMDEmaD0xNTAmZml0PWNyb3AmY29ybmVyLXJhZGl1cz0xMA%3D%3D)

