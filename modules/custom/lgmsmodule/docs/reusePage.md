# Reuse a Page

A page that is existing in some other guide can be reused again by doing the steps below.
Reusing the Page gives you the option to import all the exiting content of a page.

__Creation Date:__ March 31, 2024
__Created By:__ Mohammed Amaan


***



## Navigate to the Required guide


### 1. Click on Create/Reuse Guide Page
![Step 1 screenshot](https://images.tango.us/workflows/a964e0e7-53e1-4f98-929a-13ffbcc002e2/steps/b2924888-5eb4-4bb6-9a77-95c87a377eea/3fadcaba-127c-4cea-b6f9-90973f6a1049.png?crop=focalpoint&fit=crop&fp-x=0.1425&fp-y=0.6947&fp-z=1.9754&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=93&mark-y=455&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz00ODkmaD03MCZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 2. Click on Reuse Guide Page
![Step 2 screenshot](https://images.tango.us/workflows/a964e0e7-53e1-4f98-929a-13ffbcc002e2/steps/fec62b3f-32ea-422b-a5c8-ecd889f7e64f/ff76b24e-a179-4272-9fe4-092a13d3fc4f.png?crop=focalpoint&fit=crop&fp-x=0.3698&fp-y=0.1661&fp-z=2.2072&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=397&mark-y=303&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz00MDUmaD0xMTImZml0PWNyb3AmY29ybmVyLXJhZGl1cz0xMA%3D%3D)


### 3. Select The page to reused from existing guides
![Step 3 screenshot](https://images.tango.us/workflows/a964e0e7-53e1-4f98-929a-13ffbcc002e2/steps/83ac1753-1633-481d-874d-587e6092ea0d/00e4f53e-c52e-4f71-af74-f23f8658fec7.png?crop=focalpoint&fit=crop&fp-x=0.5005&fp-y=0.2565&fp-z=1.1706&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=102&mark-y=264&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz05OTcmaD02MSZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 4. check Include Subpages if you would also like to import the subpages of the page

Note: This option will not be available if the importing page is a subpage already.

![Step 4 screenshot](https://images.tango.us/workflows/a964e0e7-53e1-4f98-929a-13ffbcc002e2/steps/3b9137fe-0d10-44b1-96b6-0d5c1d0e4091/6b9546db-e22e-41d1-9e92-441aee6878a0.png?crop=focalpoint&fit=crop&fp-x=0.1554&fp-y=0.4371&fp-z=3.0905&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=540&mark-y=454&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz03MSZoPTcxJmZpdD1jcm9wJmNvcm5lci1yYWRpdXM9MTA%3D)


### 5. Select Link: By selecting this, a link to the HTML item will be created. it will be un-editable from this box
![Step 5 screenshot](https://images.tango.us/workflows/a964e0e7-53e1-4f98-929a-13ffbcc002e2/steps/0d1fdfe0-f036-4562-8870-2794e06568d9/bd7831df-380e-4c2e-8997-0992380921ac.png?crop=focalpoint&fit=crop&fp-x=0.1554&fp-y=0.4933&fp-z=3.0905&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=540&mark-y=454&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz03MSZoPTcxJmZpdD1jcm9wJmNvcm5lci1yYWRpdXM9MTA%3D)


### 6. Enter a Title
![Step 6 screenshot](https://images.tango.us/workflows/a964e0e7-53e1-4f98-929a-13ffbcc002e2/steps/686aaef5-7263-412c-a615-50cfce8ebebb/2e8b41ef-2c8d-40ac-b306-4cfe3d68062b.png?crop=focalpoint&fit=crop&fp-x=0.5005&fp-y=0.5864&fp-z=1.1706&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=102&mark-y=475&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz05OTcmaD02MSZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 7. Use Position to determine if it is a top level page or if it goes under an existing page.
![Step 7 screenshot](https://images.tango.us/workflows/a964e0e7-53e1-4f98-929a-13ffbcc002e2/steps/02ab8089-0e69-43ba-97bd-87eb4bae54c6/9db85022-2f86-4f55-966c-5674a95f97d3.png?crop=focalpoint&fit=crop&fp-x=0.5000&fp-y=0.5000&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n)


### 8. Click on Reuse Guide Page
![Step 8 screenshot](https://images.tango.us/workflows/a964e0e7-53e1-4f98-929a-13ffbcc002e2/steps/2d103856-4f4f-4d16-9b20-08f28797827a/b7995999-1441-49fe-a8c0-04cebb285a2a.png?crop=focalpoint&fit=crop&fp-x=0.2415&fp-y=0.7593&fp-z=2.1634&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=389&mark-y=427&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz00MjEmaD0xMjYmZml0PWNyb3AmY29ybmVyLXJhZGl1cz0xMA%3D%3D)

