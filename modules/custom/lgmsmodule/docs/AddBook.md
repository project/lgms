# Adding a Book to a Box

__Creation Date:__ March 31, 2024
__Created By:__ Mohammed Amaan



***




##  Navigate to the required box


### 1. Click on Add/Reuse Book
![Step 1 screenshot](https://images.tango.us/workflows/e33598e0-bfe8-49e0-83f0-fa057bbb015f/steps/07da3f50-9fc4-4084-a4f5-7b7bda143000/ca18dfb2-b084-46b7-90a5-af2ed85157ab.png?crop=focalpoint&fit=crop&fp-x=0.3900&fp-y=0.7626&fp-z=2.4015&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=432&mark-y=454&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz0zMzUmaD03MSZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 2. Enter the Title
![Step 2 screenshot](https://images.tango.us/workflows/e33598e0-bfe8-49e0-83f0-fa057bbb015f/steps/caa24488-411d-4c2b-9d91-3a2ea2e71931/e2bb6d54-1032-4ff9-b44b-ed2eddc6defa.png?crop=focalpoint&fit=crop&fp-x=0.5005&fp-y=0.2565&fp-z=1.1706&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=102&mark-y=264&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz05OTcmaD02MSZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 3. Enter the  Author/Editor
![Step 3 screenshot](https://images.tango.us/workflows/e33598e0-bfe8-49e0-83f0-fa057bbb015f/steps/3d96fff4-6d47-472a-8f59-528fb0fb8bbf/16b8d8a0-9f8b-4a9f-a512-ae33bcc3de3f.png?crop=focalpoint&fit=crop&fp-x=0.5005&fp-y=0.3462&fp-z=1.1706&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=102&mark-y=367&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz05OTcmaD02MSZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 4. Enter the Publisher
![Step 4 screenshot](https://images.tango.us/workflows/e33598e0-bfe8-49e0-83f0-fa057bbb015f/steps/8e95a740-ba8b-41a5-907a-55cd531ee7bd/fd37f8b0-8cad-4cd8-be31-4f72b6fdc689.png?crop=focalpoint&fit=crop&fp-x=0.5005&fp-y=0.4360&fp-z=1.1706&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=102&mark-y=460&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz05OTcmaD02MSZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 5. Enter the Year
![Step 5 screenshot](https://images.tango.us/workflows/e33598e0-bfe8-49e0-83f0-fa057bbb015f/steps/38d38619-9e38-4d0e-8134-fa733154ce9b/ec01bd18-092e-45e6-a562-5e2cf55ea75e.png?crop=focalpoint&fit=crop&fp-x=0.5005&fp-y=0.5258&fp-z=1.1706&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=102&mark-y=460&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz05OTcmaD02MSZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 6. Enter the Edition
![Step 6 screenshot](https://images.tango.us/workflows/e33598e0-bfe8-49e0-83f0-fa057bbb015f/steps/25ea8b9c-2371-40a2-90ce-cc41bfc4323f/44b06cf2-fa64-40f7-8200-7b2764f90f17.png?crop=focalpoint&fit=crop&fp-x=0.5005&fp-y=0.4360&fp-z=1.1706&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=102&mark-y=460&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz05OTcmaD02MSZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 7. Click on Cover Picture to add a cover picture ( optional )
![Step 7 screenshot](https://images.tango.us/workflows/e33598e0-bfe8-49e0-83f0-fa057bbb015f/steps/f7806d55-30dc-437b-a4de-bc9d201e19fa/0db5112d-40de-4745-8ec8-29ff452d103b.png?crop=focalpoint&fit=crop&fp-x=0.5005&fp-y=0.3805&fp-z=1.1706&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=102&mark-y=416&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz05OTcmaD00MSZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 8. Enter a discription of the book
![Step 8 screenshot](https://images.tango.us/workflows/e33598e0-bfe8-49e0-83f0-fa057bbb015f/steps/b229e287-b7e7-40f0-9f42-763541029b40/455c7d25-aec1-426e-aff3-72ec76a5ef8b.png?crop=focalpoint&fit=crop&fp-x=0.5005&fp-y=0.4175&fp-z=1.1706&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=102&mark-y=345&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz05OTcmaD0yNjgmZml0PWNyb3AmY29ybmVyLXJhZGl1cz0xMA%3D%3D)


##  If the type is Print follow the instructions below


### 9. Enter the Call Number
![Step 9 screenshot](https://images.tango.us/workflows/e33598e0-bfe8-49e0-83f0-fa057bbb015f/steps/c460dbf7-2587-4390-915a-40b69b33b5e1/3b455d73-3dce-485e-a3eb-adbe6273fef4.png?crop=focalpoint&fit=crop&fp-x=0.5005&fp-y=0.3844&fp-z=1.1706&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=102&mark-y=411&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz05OTcmaD02MSZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 10. Enter the Location
![Step 10 screenshot](https://images.tango.us/workflows/e33598e0-bfe8-49e0-83f0-fa057bbb015f/steps/87976202-c905-436b-841b-640ca9706531/918cd941-02c1-4ba0-ac83-05778521f4ee.png?crop=focalpoint&fit=crop&fp-x=0.5005&fp-y=0.4742&fp-z=1.1706&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=102&mark-y=460&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz05OTcmaD02MSZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 11. Enter the Link Link Text
![Step 11 screenshot](https://images.tango.us/workflows/e33598e0-bfe8-49e0-83f0-fa057bbb015f/steps/d650afb6-3ea3-4dc2-bb78-ea5ea8ce8881/7dbaa728-5356-4b07-9577-5ef8954727d1.png?crop=focalpoint&fit=crop&fp-x=0.5005&fp-y=0.6190&fp-z=1.1706&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=102&mark-y=513&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz05OTcmaD02MSZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 12. Enter the URL
![Step 12 screenshot](https://images.tango.us/workflows/e33598e0-bfe8-49e0-83f0-fa057bbb015f/steps/91b3fd2d-e36a-42be-9fa3-3eef35ef7f15/b6d52c58-abe4-4d25-8380-f5fc7233a80c.png?crop=focalpoint&fit=crop&fp-x=0.5005&fp-y=0.7088&fp-z=1.1706&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=102&mark-y=616&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz05OTcmaD02MSZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


##  If the type is an Ebook, follow the instructions below.


### 13. Select eBook from Type
![Step 13 screenshot](https://images.tango.us/workflows/e33598e0-bfe8-49e0-83f0-fa057bbb015f/steps/9df83f36-beff-4488-975b-d624f9daa7c5/8f1058fa-322e-470d-9281-5afddff28550.png?crop=focalpoint&fit=crop&fp-x=0.5005&fp-y=0.4574&fp-z=1.1706&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=102&mark-y=460&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz05OTcmaD02MSZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 14. Enter the  Link Text
![Step 14 screenshot](https://images.tango.us/workflows/e33598e0-bfe8-49e0-83f0-fa057bbb015f/steps/ab459363-f65c-4414-9ca5-6a930fb43906/949646a7-49e8-4d8d-ac27-d2bd3552a8fa.png?crop=focalpoint&fit=crop&fp-x=0.5005&fp-y=0.6190&fp-z=1.1706&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=102&mark-y=513&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz05OTcmaD02MSZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 15. Enter the URL to the Book
![Step 15 screenshot](https://images.tango.us/workflows/e33598e0-bfe8-49e0-83f0-fa057bbb015f/steps/a0137618-881e-407c-a2f5-4423235a3b86/b0194519-0e3c-4952-bb4a-a04833cb8bcd.png?crop=focalpoint&fit=crop&fp-x=0.5005&fp-y=0.7088&fp-z=1.1706&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=102&mark-y=616&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz05OTcmaD02MSZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 16. Click on Save to add the book to your box.
![Step 16 screenshot](https://images.tango.us/workflows/e33598e0-bfe8-49e0-83f0-fa057bbb015f/steps/18a01da9-d8e7-49d7-9b6b-398214fe418b/b70d1a09-8d43-4fdb-870a-8e2fa0ef7fc1.png?crop=focalpoint&fit=crop&fp-x=0.1962&fp-y=0.8704&fp-z=2.6918&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=485&mark-y=560&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz0yMzEmaD0xNTcmZml0PWNyb3AmY29ybmVyLXJhZGl1cz0xMA%3D%3D)

