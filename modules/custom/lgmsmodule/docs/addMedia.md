# Adding Media

The Adding Media uses another module to add libraries so the instructions on how to found on their page.

__Creation Date:__ March 31, 2024
__Created By:__ Mohammed Amaan



***




##  Navigate to the required box


### 1. Click on Add Media
![Step 1 screenshot](https://images.tango.us/workflows/9fea3130-e2fc-4ed2-9d52-fbf31825c9f5/steps/b7ade9c2-35ac-4a75-894f-17eb917be732/c1ad845a-0c14-4434-adb9-603115414357.png?crop=focalpoint&fit=crop&fp-x=0.3708&fp-y=0.7637&fp-z=2.6461&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=476&mark-y=451&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz0yNDcmaD03OSZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 2. Select the media you would like to upload by name

Additionally if you do not have any media uploaded click on create new media and then the box the says " + add new media"

The Media can be of the type

*   **Audio** A locally hosted audio file.

*   **Document** An uploaded file or document, such as a PDF.

*   **Image** Use local images for reusable media.

*   **Remote video** A remotely hosted video from YouTube or Vimeo.

*   **Video** A locally hosted video file.

![Step 2 screenshot](https://images.tango.us/workflows/9fea3130-e2fc-4ed2-9d52-fbf31825c9f5/steps/4e604438-3a05-4f8e-b075-fde56e527e87/b00e41e1-0c52-41eb-bcca-7a1a1423f3f9.png?crop=focalpoint&fit=crop&fp-x=0.5005&fp-y=0.4091&fp-z=1.1706&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=102&mark-y=439&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz05OTcmaD02MSZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 3. Uncheck Use Media Default name
![Step 3 screenshot](https://images.tango.us/workflows/9fea3130-e2fc-4ed2-9d52-fbf31825c9f5/steps/dce3754c-485c-4091-b85f-dc1bee1113f3/8e3bb108-5ecb-4dde-a0df-c361d28879a4.png?crop=focalpoint&fit=crop&fp-x=0.1554&fp-y=0.5022&fp-z=3.0905&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=540&mark-y=454&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz03MSZoPTcxJmZpdD1jcm9wJmNvcm5lci1yYWRpdXM9MTA%3D)


### 4. Add a different tittle if you wish to
![Step 4 screenshot](https://images.tango.us/workflows/9fea3130-e2fc-4ed2-9d52-fbf31825c9f5/steps/1ed70726-9407-41a5-89fa-25887d833df5/5e3d27a3-2be5-4f8c-80a0-cbaa4f7ae66a.png?crop=focalpoint&fit=crop&fp-x=0.5005&fp-y=0.5954&fp-z=1.1706&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=102&mark-y=486&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz05OTcmaD02MSZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 5. Click on Save
![Step 5 screenshot](https://images.tango.us/workflows/9fea3130-e2fc-4ed2-9d52-fbf31825c9f5/steps/8ca8087f-0fdd-448c-abe1-7bee4ffa5ecc/8e2dd2cd-c544-4e48-8be7-9a14122ac3bb.png?crop=focalpoint&fit=crop&fp-x=0.8130&fp-y=0.7637&fp-z=3.7595&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=439&mark-y=380&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz0zMjMmaD0yMTkmZml0PWNyb3AmY29ybmVyLXJhZGl1cz0xMA%3D%3D)

