# Reusing A Book

__Creation Date:__ March 31, 2024
__Created By:__ Mohammed Amaan



***




## Navigate to the box where you would like the book to be reused


### 1. Click on Add/Reuse Book
![Step 1 screenshot](https://images.tango.us/workflows/b287db8b-f524-45c7-aeaa-00376bcb25f7/steps/e7b4dd0b-73ac-4477-b33e-6c8ea6b05e65/9c3ae229-9a79-45db-b693-4a7897cd8ef0.png?crop=focalpoint&fit=crop&fp-x=0.3900&fp-y=0.6818&fp-z=2.4015&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=432&mark-y=454&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz0zMzUmaD03MSZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 2. Click on Reuse Book
![Step 2 screenshot](https://images.tango.us/workflows/b287db8b-f524-45c7-aeaa-00376bcb25f7/steps/53ef2617-298f-4040-9adb-e0cff0cad0e2/fad3cea4-62a0-46dc-bd81-4ea5cc385b9d.png?crop=focalpoint&fit=crop&fp-x=0.2864&fp-y=0.1661&fp-z=2.4445&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=440&mark-y=336&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz0zMjAmaD0xMjQmZml0PWNyb3AmY29ybmVyLXJhZGl1cz0xMA%3D%3D)


### 3. Type Book title to automatically start filtering them by the tittle

Once the required book has been selected you can go to the next step

![Step 3 screenshot](https://images.tango.us/workflows/b287db8b-f524-45c7-aeaa-00376bcb25f7/steps/3e8c7334-6dee-4645-b70d-3701aa885407/ae3dc6c5-0a99-419f-ab15-ea16c6bc7fa6.png?crop=focalpoint&fit=crop&fp-x=0.5005&fp-y=0.2565&fp-z=1.1706&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=102&mark-y=264&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz05OTcmaD02MSZmaXQ9Y3JvcCZjb3JuZXItcmFkaXVzPTEw)


### 4. Check Link: By selecting this, a link to the HTML item will be created. it will be un-editable from this box



![Step 4 screenshot](https://images.tango.us/workflows/b287db8b-f524-45c7-aeaa-00376bcb25f7/steps/51c53cbb-a57e-49e6-9be3-965b7f5afe8f/5410c3d6-3540-4356-861f-25652e99d796.png?crop=focalpoint&fit=crop&fp-x=0.1554&fp-y=0.3081&fp-z=3.0905&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=540&mark-y=454&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz03MSZoPTcxJmZpdD1jcm9wJmNvcm5lci1yYWRpdXM9MTA%3D)


### 5. Click on Save to Reuse an existing book
![Step 5 screenshot](https://images.tango.us/workflows/b287db8b-f524-45c7-aeaa-00376bcb25f7/steps/2739b876-c1f3-49e0-9626-e5326a4ec0e8/bc4af677-9395-4811-9cc7-499654738796.png?crop=focalpoint&fit=crop&fp-x=0.1962&fp-y=0.4663&fp-z=2.6918&w=1200&border=2%2CF4F2F7&border-radius=8%2C8%2C8%2C8&border-radius-inner=8%2C8%2C8%2C8&blend-align=bottom&blend-mode=normal&blend-x=0&blend-w=1200&blend64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL21hZGUtd2l0aC10YW5nby13YXRlcm1hcmstdjIucG5n&mark-x=485&mark-y=412&m64=aHR0cHM6Ly9pbWFnZXMudGFuZ28udXMvc3RhdGljL2JsYW5rLnBuZz9tYXNrPWNvcm5lcnMmYm9yZGVyPTYlMkNGRjc0NDImdz0yMzEmaD0xNTcmZml0PWNyb3AmY29ybmVyLXJhZGl1cz0xMA%3D%3D)

